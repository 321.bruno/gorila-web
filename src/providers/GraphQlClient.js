import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';

export default class GraphQlClient {
  static query(query, variables) {
    const client = new ApolloClient({
      link: new HttpLink({
        uri: process.env.VUE_APP_GRAPHQL_URL,
      }),
      cache: new InMemoryCache({
        addTypename: false,
      }),
    });

    return client.query({
      query: gql`${query}`,
      variables,
    });
  }
}
