import date from './date';

describe('[filters] date', () => {
  let mockDate;

  beforeAll(() => {
    mockDate = new Date();
    mockDate.setDate(1);
    mockDate.setMonth(0);
    mockDate.setFullYear(2018);
  });

  it('return null', () => {
    expect(date()).toBe(null);
  });

  it('return date formated with default format DD/MM/YYYY', () => {
    expect(date(mockDate)).toEqual('01/01/2018');
  });

  it('return date with custom format YYYY-MM-DD', () => {
    expect(date(mockDate, 'YYYY-MM-DD')).toEqual('2018-01-01');
  });
});
