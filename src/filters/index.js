import Vue from 'vue';
import percent from './percent';
import date from './date';
import finance from './finance';

Vue.filter('percent', percent);
Vue.filter('date', date);
Vue.filter('finance', finance);
