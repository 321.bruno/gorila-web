import Moment from 'moment';

export default function (value, format = 'DD/MM/YYYY') {
  if (!value) return null;
  const date = Moment(value);
  return date.isValid() ? date.format(format) : value;
}
