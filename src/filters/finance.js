import StringMask from 'string-mask';

export default function (input, numberPrecision = 2, decimal = ',', group = '.') {
  const precision = (!numberPrecision && numberPrecision !== 0)
  || numberPrecision < 0 ? 2 : numberPrecision;
  const decimalSep = decimal || ',';
  const groupSep = group || '.';
  const decimalsPattern = precision > 0 ? decimalSep + new Array(precision + 1).join('0') : '';
  const maskPattern = `#${groupSep}##0${decimalsPattern}`;

  let value = parseFloat(input);
  if (!value) {
    value = 0;
  }

  let negative = false;
  if (value < 0) {
    value *= -1;
    negative = true;
  }
  const financeMask = new StringMask(maskPattern, { reverse: true });
  const masked = financeMask.apply(value.toFixed(precision).replace(/[^\d]+/g, ''));
  return negative ? `(${masked})` : masked;
}
