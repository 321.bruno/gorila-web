import Vue from 'vue';

describe('[filters] index', () => {
  beforeAll(() => {
    Vue.filter = jest.fn();
    require('./index'); //eslint-disable-line
  });

  it('check register all filters', () => {
    expect(Vue.filter).toHaveBeenCalled();
  });
});
