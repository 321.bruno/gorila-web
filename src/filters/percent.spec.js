import percent from './percent';

describe('[filters] percent', () => {
  it('return null if value is null', () => {
    expect(percent(null)).toBe('0,00%');
  });

  it('return null if value isn`t number', () => {
    expect(percent('test')).toBe('0,00%');
  });

  it('return 0', () => {
    expect(percent(0)).toBe('0,00%');
  });

  it('return number if string is number', () => {
    expect(percent('123')).toBe('123,00%');
  });

  it('return number with the third decimal', () => {
    expect(percent('123.456', 3)).toBe('123,456%');
  });

  it('should round the number to max if needed', () => {
    expect(percent('123.456', 2)).toBe('123,46%');
  });

  it('should round the number to floor if needed', () => {
    expect(percent('123.454', 2)).toBe('123,45%');
  });
});
