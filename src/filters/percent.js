export default function (value, decimals = 2) {
  const percent = parseFloat(value || 0) || 0;
  return `${percent.toFixed(decimals).replace('.', ',')}%`;
}
