import finance from './finance';

describe('[filters] finance', () => {
  it('should return as defaut 0 with 2 decimals precision and , as decimal separator', () => {
    expect(finance()).toBe('0,00');
    expect(finance('')).toBe('0,00');
    expect(finance(null)).toBe('0,00');
    expect(finance(undefined)).toBe('0,00');
  });

  it('should respect decimal precision an use default when it\'s null or undefined', () => {
    expect(finance(null, 1)).toBe('0,0');
    expect(finance(null, 3)).toBe('0,000');
    expect(finance(null, undefined)).toBe('0,00');
  });

  it('should change the decimal separator and use . as default', () => {
    expect(finance(null, null, null)).toBe('0,00');
    expect(finance(null, null, '.')).toBe('0.00');
  });

  it('should change the group separator, for thousands and milions', () => {
    expect(finance(10000, null, null, ',')).toBe('10,000,00');
    expect(finance(1000000, null, null, ',')).toBe('1,000,000,00');
    expect(finance(10000, 4, '.', ',')).toBe('10,000.0000');
  });

  it('should be able to handle negative numbers', () => {
    expect(finance(-10000)).toBe('(10.000,00)');
    expect(finance(-10, null, null, ',')).toBe('(10,00)');
  });

  it('should be able to combine different configs', () => {
    expect(finance(10000, 4, ',', '.')).toBe('10.000,0000');
    expect(finance(1000000, 0, '', '.')).toBe('1.000.000');
    expect(finance(1000000, null, null, '.')).toBe('1.000.000,00');
  });
});
