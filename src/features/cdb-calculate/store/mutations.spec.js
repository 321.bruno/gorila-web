import mutations from './mutations';

describe('[mutations] cdbCalculate mutations', () => {
  it('should toggle loading', () => {
    const state = {};
    mutations.toggleLoading(state, false);
    expect(state.load).toBeFalsy();
  });

  it('should set form', () => {
    const state = {};
    mutations.setForm(state, {
      value: 1,
    });
    expect(state.form).toEqual({
      value: 1,
    });
  });

  it('should set history', () => {
    const state = {};
    mutations.setResult(state, {
      history: [{
        id: 1,
      }],
    });
    expect(state.result).toEqual({
      history: [{
        id: 1,
      }],
    });
  });
});
