import actions from './actions';
import { ToastProgrammatic as Toast } from 'buefy';
import GraphQlClient from '../../../providers/GraphQlClient';

describe('[actions] cdbCalculate actions', () => {
  let context;

  beforeAll(() => {
    Toast.open = jest.fn();
    context = {
      commit: jest.fn(),
    };
  });

  it('should have return success to calculate action', (done) => {
    GraphQlClient.query = jest.fn(() => Promise.resolve({
      data: {
        cdbCalculate: {
          history: [
            {
              date: null,
            },
          ],
        },
      },
    }));

    const form = {};
    const promise = actions.calculate(context, form);
    expect(context.commit).toHaveBeenCalledWith('setResult', null);
    expect(context.commit).toHaveBeenCalledWith('toggleLoading', true);

    promise.then((result) => {
      expect(context.commit).toHaveBeenCalledWith('toggleLoading', false);
      expect(context.commit).toHaveBeenCalledWith('setForm', form);
      expect(context.commit).toHaveBeenCalledWith('setResult', result);
      done();
    });
  });

  it('should have show toast and return success, but with empty result', (done) => {
    GraphQlClient.query = jest.fn(() => Promise.resolve({
      data: {
        cdbCalculate: {
          history: [],
        },
      },
    }));

    const form = {};
    const promise = actions.calculate(context, form);
    expect(context.commit).toHaveBeenCalledWith('setResult', null);
    expect(context.commit).toHaveBeenCalledWith('toggleLoading', true);

    promise.then(() => {
      expect(context.commit).toHaveBeenCalledWith('toggleLoading', false);
      expect(Toast.open).toHaveBeenCalledWith({
        type: 'is-warning',
        message: 'No momento não temos histórico para esse período, informe outro',
        position: 'is-bottom',
      });
      done();
    });
  });
});
