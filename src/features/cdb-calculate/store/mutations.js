export default {
  toggleLoading(state, loading) {
    state.loading = loading;
  },
  setForm(state, form) {
    state.form = form;
  },
  setResult(state, result) {
    state.result = result || {
      history: [],
    };
  },
};
