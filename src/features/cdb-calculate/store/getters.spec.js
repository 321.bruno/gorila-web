import getters from './getters';

describe('[getters] cdbCalculate getters', () => {
  let state;

  beforeAll(() => {
    state = {
      result: {
        history: [
          {
            date: '2020-07-10',
            unitPrice: 1,
          },
          {
            date: '2020-07-11',
            unitPrice: 2,
          },
        ],
      },
    };
  });

  it('should return history dates', () => {
    expect(getters.historyDates(state)).toEqual(['2020-07-10', '2020-07-11']);
  });

  it('should return unit prices', () => {
    expect(getters.historyUnitPrice(state)).toEqual([1, 2]);
  });
});
