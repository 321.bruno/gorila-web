import index from './index';

describe('[store] cdbCalculate', () => {
  it('check if name is correct', () => {
    expect(index.name).toBe('cdbCalculate');
  });

  it('should have namespaced being true', () => {
    expect(index.namespaced).toBe(true);
  });

  it('check the imports', () => {
    expect(index.state).toBeDefined();
    expect(index.mutations).toBeDefined();
    expect(index.actions).toBeDefined();
  });
});
