import { ToastProgrammatic as Toast } from 'buefy';
import GraphQlClient from '../../../providers/GraphQlClient';

export default {
  calculate({ commit }, form) {
    commit('setResult', null);
    commit('toggleLoading', true);

    return GraphQlClient.query(`
    query(
      $investmentDate: Date!
      $cdbRate: Float!
      $baseValue: Float!
      $currentDate: Date!
    ){
      cdbCalculate(
        investmentDate: $investmentDate
        cdbRate: $cdbRate
        baseValue: $baseValue
        currentDate: $currentDate
      ){
        factor
        cdiRate
        finalValue
        history {
          date
          unitPrice
        }
      }
    }`, form)
      .then((res) => {
        const {
          data: {
            cdbCalculate = {},
          } = {},
        } = res || {};
        const {
          history = [],
        } = cdbCalculate || {};

        if (history.length === 0) {
          Toast.open({
            type: 'is-warning',
            message: 'No momento não temos histórico para esse período, informe outro',
            position: 'is-bottom',
          });
          return Promise.resolve(cdbCalculate);
        }

        commit('setForm', {
          ...form,
        });

        commit('setResult', cdbCalculate);
        return Promise.resolve(cdbCalculate);
      })
      .finally(() => commit('toggleLoading', false));
  },
};
