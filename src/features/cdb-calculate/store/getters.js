export default {
  historyDates(state) {
    return state.result.history.map((item) => item.date);
  },
  historyUnitPrice(state) {
    return state.result.history.map((item) => item.unitPrice);
  },
};
