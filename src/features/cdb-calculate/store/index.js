import state from './states';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';

export default {
  name: 'cdbCalculate',
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
