import states from './states';

describe('[state] cdbCalculate states', () => {
  it('check start state', () => {
    expect(states).toEqual({
      loading: false,
      form: {},
      result: {
        history: [],
      },
    });
  });
});
