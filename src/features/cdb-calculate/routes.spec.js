import route from './routes';

describe('Route cdb-calculate', () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it('should have a name being "cdb-calculate"', () => {
    expect(route.name).toBe('cdb-calculate');
  });

  it('should have a path being "/"', () => {
    expect(route.path).toBe('/');
  });

  it('should receive the PageCdbCalculate as component', (done) => {
    jest.doMock('./PageCdbCalculate', () => {
      return {
        __esModule: true,
        default: {
          name: 'page-cdb-calculate',
        },
      };
    });

    route.component().then((result) => {
      expect(result.default.name).toBe('page-cdb-calculate');
      done();
    });
  });

  it('should have a title being "Cálculo CDB pós fixado"', () => {
    expect(route.meta.title).toEqual('Cálculo CDB pós fixado');
  });
});
