const PageCdbCalculate = () => import('./PageCdbCalculate');

export default {
  path: '/',
  name: 'cdb-calculate',
  component: PageCdbCalculate,
  meta: {
    title: 'Cálculo CDB pós fixado',
  },
};
