import { shallowMount } from '@vue/test-utils';
import Vuex from 'vuex';
import PageCdbCalculate from './PageCdbCalculate.vue';

describe('PageCdbCalculate.vue', () => {
  let store;
  let wrapper;
  let vm;

  beforeAll(() => {
    store = new Vuex.Store({
      modules: {
        cdbCalculate: {
          state: {
            loading: false,
          },
        },
      },
    });

    wrapper = shallowMount(PageCdbCalculate, {
      store,
    });

    vm = wrapper.vm;
  });

  it('check name component', () => {
    expect(vm.$options.name).toEqual('page-cdb-calculate');
  });
});
