import Vue from 'vue';
import VueRouter from 'vue-router';
import PageCdbCalculate from '../features/cdb-calculate/routes';

Vue.use(VueRouter);

const routes = [
  PageCdbCalculate,
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const {
    meta: {
      title = '',
    } = {},
  } = to;

  document.title = `${title} | Gorila`;

  next();
});

export default router;
