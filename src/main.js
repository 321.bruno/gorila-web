import Vue from 'vue';
import Buefy from 'buefy';
import App from './App.vue';
import router from './router';
import store from './store';
import './filters';
import './vee-validate';

Vue.use(Buefy);

Vue.config.productionTip = false;
Vue.prototype.$filters = Vue.options.filters;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
