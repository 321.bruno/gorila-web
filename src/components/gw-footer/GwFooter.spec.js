import { mount } from '@vue/test-utils';
import GwFooter from './GwFooter.vue';

describe('[components] GwFooter.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(GwFooter);
  });

  it('check class gw-footer in footer', () => {
    const footer = wrapper.find('footer');
    expect(footer.attributes('class')).toBe('gw-footer');
  });

  it('check copyright text', () => {
    const small = wrapper.find('small');
    expect(small.text()).toBe(`© ${new Date().getFullYear()} Todos os Direitos Reservados`);
  });
});
