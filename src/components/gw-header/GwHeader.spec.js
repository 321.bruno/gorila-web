import { mount } from '@vue/test-utils';
import GwHeader from './GwHeader.vue';

describe('[components] GwHeader.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(GwHeader);
  });

  it('check class gw-header in header', () => {
    const header = wrapper.find('header');
    expect(header.attributes('class')).toBe('gw-header');
  });

  it('check src and alt logo', () => {
    const logo = wrapper.find('img.gw-header__logo');
    expect(logo.attributes('src')).toBe('../../assets/logo.png');
    expect(logo.attributes('alt')).toBe('Gorila');
  });

  it('check slogan text', () => {
    const slogan = wrapper.find('small.gw-header__slogan');
    expect(slogan.text()).toEqual(`Calcule o
    CDB pós fixado indexado ao
    CDI`);
  });
});
