import { shallowMount } from '@vue/test-utils';
import { BField } from 'buefy/dist/components/field';
import GwField from './GwField.vue';

describe('[components] GwField.vue', () => {
  let wrapper;
  let vm;

  beforeEach(() => {
    wrapper = shallowMount(GwField);
    vm = wrapper.vm;
  });

  it('check name component', () => {
    expect(vm.$options.name).toEqual('GwField');
  });

  it('check extends', () => {
    expect(GwField.extends).toEqual(BField);
  });
});
