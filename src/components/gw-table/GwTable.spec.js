import { shallowMount } from '@vue/test-utils';
import { BTable } from 'buefy/dist/components/table';
import GwTable from './GwTable.vue';

describe('[components] GwTable.vue', () => {
  let wrapper;
  let vm;

  beforeEach(() => {
    wrapper = shallowMount(GwTable);
    vm = wrapper.vm;
  });

  it('check name component', () => {
    expect(vm.$options.name).toEqual('GwTable');
  });

  it('check extends', () => {
    expect(GwTable.extends).toEqual(BTable);
  });
});
