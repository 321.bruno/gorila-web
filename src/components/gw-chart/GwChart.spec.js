import { shallowMount } from '@vue/test-utils';
import { Chart } from 'highcharts-vue';
import GwChart from './GwChart.vue';

describe('[components] GwChart.vue', () => {
  let wrapper;
  let vm;

  beforeEach(() => {
    wrapper = shallowMount(GwChart, {
      propsData: {
        options: {},
      },
    });
    vm = wrapper.vm;
  });

  it('check name component', () => {
    expect(vm.$options.name).toEqual('GwChart');
  });

  it('check extends', () => {
    expect(GwChart.extends).toEqual(Chart);
  });
});
