import { mount } from '@vue/test-utils';
import GwEmpty from './GwEmpty.vue';

describe('[components] GwEmpty.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(GwEmpty);
  });

  it('check class gw-empty in div', () => {
    const div = wrapper.find('div');
    expect(div.attributes('class')).toBe('gw-empty');
  });

  it('check alt and src of image', () => {
    const div = wrapper.find('img');
    expect(div.attributes('src')).toBe('../../assets/empty.svg');
    expect(div.attributes('alt')).toBe('Nenhum resultado');
  });
});
