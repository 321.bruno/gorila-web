import { shallowMount } from '@vue/test-utils';
import { BInput } from 'buefy/dist/components/input';
import GwInput from './GwInput.vue';

describe('[components] GwInput.vue', () => {
  let wrapper;
  let vm;

  beforeEach(() => {
    wrapper = shallowMount(GwInput);
    vm = wrapper.vm;
  });

  it('check name component', () => {
    expect(vm.$options.name).toEqual('GwInput');
  });

  it('check extends', () => {
    expect(GwInput.extends).toEqual(BInput);
  });
});
