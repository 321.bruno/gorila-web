import { shallowMount } from '@vue/test-utils';
import GwCurrency from '../gw-currency/GwCurrency.vue';
import GwPercentage from './GwPercentage.vue';

describe('[components] GwPercentage.vue', () => {
  let wrapper;
  let vm;

  beforeEach(() => {
    wrapper = shallowMount(GwPercentage, {
      directives: {
        currency: {},
      },
    });
    vm = wrapper.vm;
  });

  it('check name component', () => {
    expect(vm.$options.name).toEqual('GwPercentage');
  });

  it('check extends', () => {
    expect(GwPercentage.extends).toEqual(GwCurrency);
  });
});
