import { shallowMount } from '@vue/test-utils';
import { BDatepicker } from 'buefy/dist/components/datepicker';
import GwDatepicker from './GwDatepicker.vue';

describe('[components] GwDatepicker.vue', () => {
  let wrapper;
  let vm;

  beforeEach(() => {
    wrapper = shallowMount(GwDatepicker);
    vm = wrapper.vm;
  });

  it('check name component', () => {
    expect(vm.$options.name).toEqual('GwDatepicker');
  });

  it('check override default props', () => {
    expect(vm.$props.placeholder).toEqual('DD/MM/YYYY');
    expect(vm.$props.icon).toEqual('calendar-today');
    expect(vm.$props.dateFormatter([2020, 6, 17])).toEqual('17/07/2020');
    expect(vm.$props.dayNames).toEqual([
      'Dom',
      'Seg',
      'Ter',
      'Qua',
      'Qui',
      'Sex',
      'Sab',
    ]);
    expect(vm.$props.monthNames).toEqual([
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro',
    ]);
  });

  it('check extends', () => {
    expect(GwDatepicker.extends).toEqual(BDatepicker);
  });
});
