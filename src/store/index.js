import Vue from 'vue';
import Vuex from 'vuex';
import cdbCalculate from '../features/cdb-calculate/store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    cdbCalculate,
  },
});
