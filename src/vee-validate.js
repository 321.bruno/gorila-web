import { localize, extend } from 'vee-validate';
import ptBr from 'vee-validate/dist/locale/pt_BR.json';
import * as rules from 'vee-validate/dist/rules';

Object.keys(rules).forEach((rule) => {
  extend(rule, rules[rule]);
});

localize('pt_BR', ptBr);
