module.exports = {
  verbose: true,
  preset: '@vue/cli-plugin-unit-jest',
  testMatch: [
    '**/*.spec.[jt]s?(x)',
  ],
  setupFiles: [
    '<rootDir>/test/unit/setup',
  ],
  coverageDirectory: 'test/unit/coverage',
  collectCoverageFrom: [
    'src/**/*.{js,vue}',
    '!src/**/*.spec.{js,vue}',
    '!src/main.js',
    '!src/router/index.js',
    '!src/store/index.js',
    '!**/node_modules/**',
    '!**/test/**',
  ],
  coverageReporters: ['json', 'lcov', 'text', 'html'],
};
