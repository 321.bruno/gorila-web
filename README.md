# Gorila Web
https://d2lhu97aqgpxxd.cloudfront.net/

Painel web para fazer o cáculo 

## Stack
* VueJS
* Vuex
* GraphQL Client
* Highcharts

### Requisitos
 * Node 10.x
 * Yarn 1.19.x

### Instalação

```shell script
yarn
```

### Desenvolvimento

```shell script
yarn serve
```

### Testes

```shell script
yarn test:unit
```
### Lint

```shell script
yarn lint
```

### Produção

```shell script
yarn build
```

### CI/CD e Hospedagem

* Gitlab (.gitlab-ci.yml)
* AWS S3 + CloudFront (configurações no Variables do Gitlab)

### TODOs

* Finalizar os testes unitários
* Teste e2e
